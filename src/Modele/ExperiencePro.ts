
export default interface ExperiencePro {
  date: string
  company: string
  attribution: string
  description: string
}