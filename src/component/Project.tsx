import React from 'react';

import {FormattedMessage} from 'react-intl';

import "../assets/css/Project.css";

//import flashCard1 from '../assets/img/flashCard1.jpg';
import flashCard2 from '../assets/img/flashCard2.jpg';
import rgbAT from '../assets/img/rgb-at.jpg';

class Project extends React.Component{
    render =()=>{
         return(
        <div className='box h2_style_util' id="project">
            <h2 className='h2_project_style'>
                <i className='fas fa-folder-open mx-3'></i>
                <FormattedMessage id = "menu.m4"
                defaultMessage = "Projects" 
                />
            </h2>

            <div className="row row-cols-1 row-cols-md-2 g-4">
  <div className="col" id="project1">
    <div className="card card_container">
      < img src={flashCard2} aria-hidden alt="Responsive image" className="card-img-top img_style_project" />
      <div className="card-body ">
        <h5 className="card-title">FlashCard</h5>
        < p className="card-text">
            <FormattedMessage id = "project.flashcardDescription"
            defaultMessage = "A web app that help to memorize cards, a card corresponds to an image, so it becomes more easier to remember a picture than a number. The card flips so we try to figure out what number is behind the picture" 
            />
            <p><a href="https://drive.google.com/file/d/1GvJ_gUwWhwbi6ThtZfJxNa3N1ojFS_cg/view?usp=drivesdk">Demo1 FlashCard</a>  <br />
            <a href="https://drive.google.com/file/d/1GwrlZheZPM0nkdarSXOqXDwitpujpZgL/view?usp=drivesdk">Demo2 FlashCard</a></p>

        </p>
      </div>
    </div>
  </div>
  <div className="col" id="project2">
    <div className="card card_container">
    < img src={rgbAT} aria-hidden alt="Responsive image" className="card-img-top img_style_project"/>
      <div className="card-body ">
        <h5 className="card-title">
              <FormattedMessage id = "project.rgbAT"
              defaultMessage = "Automatical selector of package" 
              />
        </h5>
        <p className="card-text">
            <FormattedMessage id = "project.rgbATDescription"
            defaultMessage = "Used arduino with some electronic components and programing, the project consists to make a prototype of a tool which selects packages by their color, can be improved if we want to select fruits" 
            />
            <p><a href="https://docs.google.com/presentation/d/1HV8F0G2Cn7oNikXnpUmPRGyL3QddXs2t/edit?usp=drivesdk&ouid=111549935565097364658&rtpof=true&sd=true">Slide RGB-AT</a></p>
            <br />
        </p>
      </div>
    </div>
  </div>

 
</div>

        </div>
    )
    }
   
}
export default Project;
