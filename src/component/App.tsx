import React, {useContext} from 'react';
import {FormattedMessage} from 'react-intl';

import {IntlProvider} from 'react-intl'
import French from '../i18n/lang/fr.json'
import English from '../i18n/lang/en.json'

import Project from '../component/Project';
import Parcours from '../component/Parcours';
import Fiche from '../component/Fiche';
import InfoPers from '../component/InfoPers';
import Techno from '../component/Techno';
import Contact from '../component/Contact';

import frenchFlag from '../assets/img/french.jpg';
import englishFlag from '../assets/img/english.png';

import "../assets/css/Menu.css";
import "../assets/css/Fiche.css";

import "../assets/bootstrap-5.0.2-dist/css/bootstrap.min.css"
// import "../assets/S-5.0.2-dist/js/bootstrap.bundle.js"
import "../assets/bootstrap-5.0.2-dist/fontawesome-free-6.1.1-web/css/all.css"

const locale = navigator.language;
    let lang;
    if (locale==="en") {
      lang = English;
    } else {
      lang = French;
    }

    class App extends React.Component{
    
        state = {
            language_name : 'French',
            language : French,
            change_language_name : 'To English',
            flag : 'englishFlag'
        }

        switchLangHandler = () =>{
            console.log('was clicked ! ')
        let newState = {
            language_name : 'English',
            language : English,
            change_language_name : 'To French',
            flag : frenchFlag
        }
        if (this.state.language_name == 'English'){
            newState.language_name = 'French';
            newState.language = French;
            newState.change_language_name = 'To English';
            newState.flag = englishFlag
        }
        this.setState(newState) ; 
        }
        render = () => {
            return (
                <div>  

                    <IntlProvider locale = {locale} messages = {this.state.language}>

                    <nav className="navbar navbar-expand-lg fixed-top navbar-light menu_container ">
              <div className="container-fluid">
                <a className="navbar-brand btn btn-outline-primary active " href="#aboutMe">
                <FormattedMessage id = "menu.m1"
                defaultMessage = "About me" 
                />
                </a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                      <a className="nav-link  btn btn-outline-primary me-3 " aria-current="page" href="#parcours">
                        <FormattedMessage id = "menu.m2"
                        defaultMessage = "Studies" 
                        />
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link  btn btn-outline-primary me-3 " aria-current="page" href="#techno">
                        <FormattedMessage id = "menu.m3"
                        defaultMessage = "Technologies used" 
                        />
                      </a>
                    </li>
                   
                    <li className="nav-item dropdown">
                      <a className="nav-link dropdown-toggle btn btn-outline-primary me-3 " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <FormattedMessage id = "menu.m4"
                        defaultMessage = "Projects" 
                        />
                      </a>
                      <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a className="dropdown-item " href="#project1">FlashCard</a></li>
                        <li><a className="dropdown-item " href="#project2">Arduino project</a></li>
                      </ul>
                    </li>
                     <li className="nav-item">
                      <a className="nav-link btn btn-outline-primary me-3 " aria-current="page" href="#contact">Contact</a>
                    </li>
                    <li className="nav-item">
                      <a onClick={this.switchLangHandler} className="btn btn-outline-secondary  " aria-current="page" >
                      <img src={this.state.flag} aria-hidden className="icon" alt=""/>
                          {this.state.change_language_name}</a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
                                                       
                    <Fiche/> 
                    <InfoPers/>
                    <Parcours/>
                    <Techno/>
                    <Project/>
                    <Contact/>
                    </IntlProvider>        
                </div>
            )
    
        }
    }
    
    export default App;

