import React from 'react';
import {FormattedMessage} from 'react-intl';

import photo from '../assets/img/gianie.jpg';
import "../assets/css/Fiche.css";


// import "../assets/bootstrap-5.0.2-dist/css/bootstrap.min.css"
// import "../assets/bootstrap-5.0.2-dist/fontawesome-free-6.1.1-web/css/all.css"

class Fiche extends React.Component{
    render =()=>{
         return(
        <div className='Fiche_container'>
            <img src={photo} aria-hidden className=" img_style" alt="Responsive image"/>
        <div className='h2_style_fiche'>
            <h3> Gianie RAMANANJATO</h3> 
            <p>
                <FormattedMessage id = "fiche.titre"
                defaultMessage = "Junior Developer Fullstack" 
                />
            </p>
            <p>
                <FormattedMessage id = "fiche.specify"
                defaultMessage = "JavaScript Developer" 
                />
            </p>
            <h3 >
                <a href="mailto:gianierama@gmail.com"><i className=" fas fa-envelope mx-3"></i></a>
                <a href="https://www.facebook.com/philibertgianie.ramananjato"><i className="fab fa-facebook fa-style mx-3"></i></a>
                <a href="https://www.linkedin.com/in/philibert-gianie-ramananjato-5a0195169"><i className="fab fa-linkedin fa-style mx-3"></i></a>
                <a href="https://gitlab.com/gianieCJ7"><i className="fab fa-gitlab fa-style mx-3"></i></a>
            </h3>
            
            
        </div>

        </div>
    )
    }
   
}
export default Fiche


