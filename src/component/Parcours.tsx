import React from 'react';
import {FormattedMessage} from 'react-intl';

import "../assets/css/Parcours.css";
import "../assets/css/util.css";


class Parcours extends React.Component{
    render =()=>{
         return(
        <div className=' box h2_style_util' id="parcours">
            <h2 className='h2_parcours_style'><i className='fas fa-school-flag mx-3'></i> 
            <FormattedMessage id = "parcours.titre"
            defaultMessage = "ACADEMIC BACKGROUND" 
            />
            </h2>
              
                <h5>{'>>'}
                <FormattedMessage id = "parcours.fromationODC"
                defaultMessage = "Learning | Become a Junior Fullstack Developer" 
                />
                </h5>                       
                    <p>Orange Digital Center Madagascar | 02/2022 - 04/2022</p>
                <h5>{'>>'}
                <FormattedMessage id = "parcours.bachelorCNTEMAD"
                defaultMessage = "Bachelor in Computer Sciences and Data Base" 
                />
                </h5>
                    <p>Centre National de Télé-Enseignement de Madagascar | 2018-present</p>
                <h5>{'>>'}
                <FormattedMessage id = "parcours.masterUCM"
                defaultMessage = "Master in Macroeconomics" 
                />
                </h5>                   
                    <p>Universite Catholique de Madagascar | 2013-2018</p>
                
        </div>       
    )
    }
}
export default Parcours;

