import React from 'react';
import {FormattedMessage} from 'react-intl';

import "../assets/css/Techno.css";
import "../assets/css/util.css";
// import "../assets/bootstrap-5.0.2-dist/css/bootstrap.min.css"
// import "../assets/bootstrap-5.0.2-dist/js/bootstrap.bundle.js"
// import "../assets/bootstrap-5.0.2-dist/fontawesome-free-6.1.1-web/css/all.css"

class Techno extends React.Component{
    render =()=>{
         return(
        <div className='box h2_style_util' id="techno">
            <h2 className='h2_techno_style'>
                <i className='fas fa-laptop-code mx-3'></i>
                <FormattedMessage id = "menu.m3"
                defaultMessage = "Technologies used" 
                />
            </h2>
                <ul className="nav justify-content-center ">
                    <li className="nav-item">
                    <a  className=" button_techno">HTML5</a>
                    </li>
                    <li className="nav-item">
                    <a   className=" button_techno">CSS3</a>
                    </li>
                    <li className="nav-item">
                    <a   className=" button_techno">Bootstrap 5.1</a>
                    </li>
                    <li className="nav-item">
                    <a   className=" button_techno">JS ES6</a>
                    </li>
                </ul>

                <ul className="nav justify-content-center h2_techno_style ">
                    <li className="nav-item">
                    <a  className=" button_techno">ReactJS</a>
                    </li>
                    <li className="nav-item">
                    <a   className=" button_techno">NodeJS</a>
                    </li>
                    <li className="nav-item">
                    <a   className=" button_techno">MySQL/SQL</a>
                    </li>
                    <li className="nav-item">
                    <a   className=" button_techno">PHP</a>
                    </li>
                </ul> 
         </div>
    )
    }
   
}
export default Techno;
