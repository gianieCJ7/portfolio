import React from 'react';

import {FormattedMessage} from 'react-intl';

import "../assets/css/InfoPers.css";
import "../assets/css/util.css";
// import "../assets/bootstrap-5.0.2-dist/css/bootstrap.min.css"
// import "../assets/bootstrap-5.0.2-dist/fontawesome-free-6.1.1-web/css/all.css"
    


class InfoPers extends React.Component{
    render =()=>{
       
         return(
        <div className='box h2_style_util' id="aboutMe">

            <h2 className='h2_infoPers_style'><i className="fas fa-female fa-style mx-3"/>  
                <FormattedMessage id = "infoPers.titre"
                defaultMessage = "ABOUT ME" 
                />
            </h2>
            <h5>RAMANANJATO Gianie</h5>
           
            <p >
                <FormattedMessage id = "infoPers.p1"
                defaultMessage = "I'm a MERN stack developer," 
                />
            </p>
            <p >
                <FormattedMessage id = "infoPers.p2"
                defaultMessage = "I pay attention both in design and functionalities when making an app" 
                />
            </p>
            <p >
                <FormattedMessage id = "infoPers.p3"
                defaultMessage = "Like learning new things and sharing what I can share to help others" 
                />
                </p>

        </div>
    )
    }
   
}
export default InfoPers;
