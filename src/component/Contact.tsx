import React from 'react';
import {FormattedMessage} from 'react-intl';

import "../assets/css/Contact.css";


class Contact extends React.Component{
    render =()=>{
         return(
            <div className='box h2_style_util' id="contact">
                <h2 className='h2_contact_style'>
                <i className='fas fa-contact-card mx-3'></i>
                <FormattedMessage id = "contact.titre"
                defaultMessage = "To contact me" 
                />
                </h2>     
                <h3 className='h3_contact_style'>
                <a href="mailto:gianierama@gmail.com"><i className=" fas fa-envelope mx-3"></i></a>
                <a href="https://www.facebook.com/philibertgianie.ramananjato"><i className="fab fa-facebook fa-style mx-3"></i></a>
                <a href="https://www.linkedin.com/in/philibert-gianie-ramananjato-5a0195169"><i className="fab fa-linkedin fa-style mx-3"></i></a>
                <a href="https://gitlab.com/gianieCJ7"><i className="fab fa-gitlab fa-style mx-3"></i></a>
                </h3>
            </div>
           
    )
    }
   
}
export default Contact;
