import React from 'react'
import ReactDOM from 'react-dom'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle.js'
import App from '../src/component/App'

// import './i18n/i18n';
// import i18next from './i18n/i18n'


// i18next.t('my.key');

    ReactDOM.render(
      <React.StrictMode>
        <App/>
      </React.StrictMode>,
      document.getElementById('root')
    );


// serviceWorker.unregister ();
